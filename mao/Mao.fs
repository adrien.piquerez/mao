﻿module Mao

type Player = Player of string
type Suit = Spade | Club | Heart | Diamond
type Rank = Rank of int | Jack | Queen | King
type Card = Rank * Suit
type Command = PlayCard 